# Object-Oriented Programming Languages III 2016/17 final project #

This repository is for students of the Object-Oriented Programming Languages III course final laboratory project in years 2016/17. Only through contribution via fork of this repository project will be considered submitted.

## Basic project rules

* Each student need to create a Java application using Java SE programming language.
* Projects need to be submitted till January 22, 11:59:59 PM (BitBucket server time). All contributions made after deadline will result in failing project (hence failing course).
* Submitting project before deadline is required to be admitted to oral exam.
* Not submitting project before deadline will result in failing laboratories (hence failing course).
* Subject (what the application actually does) it totally up to student. If for any reason student has a problem with choosing application subject, he or she can ask for assistance project verificators.
* Each student is obliged to create fork of this repository and make all contributions to it. Contributions made to original repository (f.e. pull requests) or send via other channels (like email) won't be accepted.
* Only source code, resources and so should be pushed to repository.
* Application must be created using Maven (version 3.X.Y) and project verificators must be able to build it using standard commands (like 'mvn compile' and 'mvn package').
* Each project needs to be documented by README.md file (edited version of this one). Documentation needs to contain:
	* project author name,
	* description of what project does (in just a few sentences),
	* instructions how to compile and run application,
	* all additional information needed for application usage.

## Requirements for project

* All source code needs to be created regarding [Java Code Conventions](http://www.oracle.com/technetwork/java/codeconventions-150003.pdf).
* Project needs to have at least 2-level class inheritance present (regardless if base class is abstract or not).
* Example of using class field polymorphic binding needs to be present in the source code.
* Usage of at least two custom interfaces is mandatory.
* Project code needs to use two types of collections: List or Set and Map. Collections implementation is for student to decide.
* Example of collection iteration needs to be present in the source code.
* Custom exception needs to be created used in the project.

## Project verificators

* Dr. Jan Franz
* Dr. hab. Julien Guthmuller

## Forking repository

All needed information about forking a repository hosted on BitBucket can be found in the [documentation](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

If there is a problem, please contact project verificators via email or during laboratories.


1. Jarosław Kołaczek
2. Project has all the things that were included in requirements. Main class is Animal, subclass is human and horse. Next is stallion and mare. Mare includes 2 exceptions with interface.
Also class human have an interface. List, mapping and polymorphism are included in main.
3. Standard - compile all and run main. 
4. There is a possibility to change exceptions changing false or true in main where program creates Mare.
