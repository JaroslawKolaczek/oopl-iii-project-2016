package animals;

abstract public class Horse extends Animal
{
	int gallopSpeed;
	
	abstract void gallop() throws IsPregnant;
	
	@Override
	public String toString() 
	{
		return this.name;
	}
	
}
