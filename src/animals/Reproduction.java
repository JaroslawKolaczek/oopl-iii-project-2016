package animals;

public interface Reproduction
{
	boolean getPregnant();
	void setPregnant(boolean pregnant);
	boolean getInLove();
	
	default void colt() throws IsPregnant, Lonely
	{
		if (getPregnant())
		{
			throw new IsPregnant();
		}
		else if (!getInLove())
		{
			throw new Lonely();
		}
		else
		{
			setPregnant(true);
			System.out.println("HURAY! Reproduction completed !");
		}
	}
}
