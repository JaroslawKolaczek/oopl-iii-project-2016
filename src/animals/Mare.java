package animals;

public class Mare extends Horse implements Reproduction
{
	boolean pregnant;
	boolean inLove;

	@Override
	void gallop() throws IsPregnant
	{
		if (this.pregnant)
		{
			throw new IsPregnant();
		}
		System.out.println("Mare is patataing !");
		
	}
	Mare(String name, boolean pregnant, boolean inLove)
	{
		this.pregnant = pregnant;
		this.gallopSpeed = 60;
		this.name = name;
		this.age = 0;	
		this.inLove = inLove;
	}
	@Override
	public boolean getPregnant() 
	{
		return this.pregnant;
	}
	@Override
	public void setPregnant(boolean pregnant) 
	{
		this.pregnant = pregnant;
		
	}
	@Override
	public boolean getInLove() 
	{
		return this.inLove;
	}
}
