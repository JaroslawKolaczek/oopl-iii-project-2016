package animals;

public class Human extends Animal implements LivePath
{
	int iq;
	
	Human(String name, int iq)
	{
		this.iq = iq;
		this.age = 0;
		this.name = name;
	}

	@Override
	public int getIq() 
	{
	
		return this.iq;
	}
	
	
	
}
