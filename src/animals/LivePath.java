package animals;

public interface LivePath 
{
	int getIq();
	
	default void way()
	{
		if (getIq() > 100)
		{
			System.out.println("PG is waiting for you!");
		}
		else
			System.out.println("WSB is better for you!");
	}
	
}
