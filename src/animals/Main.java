package animals;
import java.util.*;

public class Main {

	public static void main(String[] args) 
	{
		Human Andrzej = new Human("Andrzej", 101);
		
		Andrzej.way();
		
		// First false - if pregnant, second false - if inLove
		Mare Betty = new Mare("Betty", false, true);
		
		Betty.setAge(8);
		Betty.getAge();
		
		// 2 exceptions here
		try 
		{
			Betty.colt();
		} 
		catch (IsPregnant | Lonely e) 
		{
			System.out.println(e.getMessage());
		}
		try 
		{
			Betty.gallop();
		} 
		catch (IsPregnant e) 
		{
			
			System.out.println(e.getMessage());
		}
		
		//List here with polymorphism 
		Horse[] HorsesNames = new Horse[10];
		HorsesNames[0] = new Mare("Betty", false, true);
		HorsesNames[1] = new Mare("Bryza", false, true);
		HorsesNames[2] = new Mare("Bella", false, true);
		HorsesNames[3] = new Mare("Samba", false, true);
		HorsesNames[4] = new Mare("Laguna", false, true);
		HorsesNames[5] = new Mare("Galia", false, true);
		HorsesNames[6] = new Mare("Rose", false, true);
		HorsesNames[7] = new Stallion("Orion");
		HorsesNames[8] = new Stallion("Dominion");
		HorsesNames[9] = new Stallion("Prince");
		
		List<String> list = new ArrayList<String>();

		for(int i = 0; i < 10; i++)
		{
			list.add(HorsesNames[i].name);
		}
		for (Horse h:HorsesNames)
		{
			try 
			{
				h.gallop();
			} 
			catch (IsPregnant e) 
			{
				e.printStackTrace();
			}
		}
			
		System.out.println(list);
		
		//Mapping
		HashMap<Horse, Integer>	Price = new HashMap<>();
		
	
		Price.put(HorsesNames[0], 39999);
		Price.put(HorsesNames[1], 23000);
		Price.put(HorsesNames[2], 15000);
		Price.put(HorsesNames[3], 48000);
		Price.put(HorsesNames[4], 18000);
		Price.put(HorsesNames[5], 12000);
		Price.put(HorsesNames[6], 40000);
		Price.put(HorsesNames[7], 90000);
		Price.put(HorsesNames[8], 120000);
		Price.put(HorsesNames[9], 75000);	
		
		for (HashMap.Entry<Horse,Integer> entry : Price.entrySet()) 
		{
			System.out.println(entry.getKey() + " -- Price: " + entry.getValue() + "$");
		}

	}

}
