package animals;

abstract public class Animal 
{
	String name;
	int age;
	
	void getAge()
	{
		System.out.println("Betty is " + this.age + "!");
	}
	
	void setAge (int age)
	{
		this.age = age;
	}
	
}
